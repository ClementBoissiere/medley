package mvc.dao;

import mvc.models.ArticleEntity;
import mvc.utils.ConnectionDB;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArticleDAO {

    ConnectionDB connectionDB = ConnectionDB.getInstance();
    
    Session entityManager = connectionDB.openConnection();
    
    public List<ArticleEntity> getArticles(){
        List<ArticleEntity> articleEntityList = new ArrayList<ArticleEntity>();
        try {
            entityManager.beginTransaction();

            List<ArticleEntity> listArticle = entityManager.createQuery("FROM ArticleEntity").getResultList();
            for (Iterator<ArticleEntity> iterator = listArticle.iterator(); iterator.hasNext(); ) {
                ArticleEntity article = iterator.next();
                articleEntityList.add(article);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return articleEntityList;
    }
    
    public ArticleEntity getArcticleById(Integer id){
        ArticleEntity articleEntity = new ArticleEntity();
        try {
            entityManager.beginTransaction();

            articleEntity = entityManager.find(ArticleEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return articleEntity;
    }

}
