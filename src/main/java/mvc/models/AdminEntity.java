package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "admin", schema = "MEDLEY_BD_OSS3", catalog = "")
public class AdminEntity {
    private Integer nbDeletedComment;
    private int idUser;

    @Basic
    @Column(name = "nb_deleted_comment")
    public Integer getNbDeletedComment() {
        return nbDeletedComment;
    }

    public void setNbDeletedComment(Integer nbDeletedComment) {
        this.nbDeletedComment = nbDeletedComment;
    }

    @Id
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminEntity that = (AdminEntity) o;

        if (idUser != that.idUser) return false;
        if (nbDeletedComment != null ? !nbDeletedComment.equals(that.nbDeletedComment) : that.nbDeletedComment != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nbDeletedComment != null ? nbDeletedComment.hashCode() : 0;
        result = 31 * result + idUser;
        return result;
    }
}
