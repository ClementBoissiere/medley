package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "create_answer", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(CreateAnswerEntityPK.class)
public class CreateAnswerEntity {
    private Byte answer;
    private Byte createP;
    private int idUser;
    private int idArticle;

    @Basic
    @Column(name = "answer")
    public Byte getAnswer() {
        return answer;
    }

    public void setAnswer(Byte answer) {
        this.answer = answer;
    }

    @Basic
    @Column(name = "create_p")
    public Byte getCreateP() {
        return createP;
    }

    public void setCreateP(Byte createP) {
        this.createP = createP;
    }

    @Id
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Id
    @Column(name = "id_article")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreateAnswerEntity that = (CreateAnswerEntity) o;

        if (idUser != that.idUser) return false;
        if (idArticle != that.idArticle) return false;
        if (answer != null ? !answer.equals(that.answer) : that.answer != null) return false;
        if (createP != null ? !createP.equals(that.createP) : that.createP != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = answer != null ? answer.hashCode() : 0;
        result = 31 * result + (createP != null ? createP.hashCode() : 0);
        result = 31 * result + idUser;
        result = 31 * result + idArticle;
        return result;
    }
}
