package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "difficulty", schema = "MEDLEY_BD_OSS3", catalog = "")
public class DifficultyEntity {
    private int idDifficulty;
    private String nameDifficulty;
    private Integer difficultyPoint;

    @Id
    @Column(name = "id_difficulty")
    public int getIdDifficulty() {
        return idDifficulty;
    }

    public void setIdDifficulty(int idDifficulty) {
        this.idDifficulty = idDifficulty;
    }

    @Basic
    @Column(name = "name_difficulty")
    public String getNameDifficulty() {
        return nameDifficulty;
    }

    public void setNameDifficulty(String nameDifficulty) {
        this.nameDifficulty = nameDifficulty;
    }

    @Basic
    @Column(name = "difficulty_point")
    public Integer getDifficultyPoint() {
        return difficultyPoint;
    }

    public void setDifficultyPoint(Integer difficultyPoint) {
        this.difficultyPoint = difficultyPoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DifficultyEntity that = (DifficultyEntity) o;

        if (idDifficulty != that.idDifficulty) return false;
        if (nameDifficulty != null ? !nameDifficulty.equals(that.nameDifficulty) : that.nameDifficulty != null)
            return false;
        if (difficultyPoint != null ? !difficultyPoint.equals(that.difficultyPoint) : that.difficultyPoint != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDifficulty;
        result = 31 * result + (nameDifficulty != null ? nameDifficulty.hashCode() : 0);
        result = 31 * result + (difficultyPoint != null ? difficultyPoint.hashCode() : 0);
        return result;
    }
}
