package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "article_category", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(ArticleCategoryEntityPK.class)
public class ArticleCategoryEntity {
    private int idArticle;
    private int idCategory;

    @Id
    @Column(name = "id_article")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Id
    @Column(name = "id_category")
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleCategoryEntity that = (ArticleCategoryEntity) o;

        if (idArticle != that.idArticle) return false;
        if (idCategory != that.idCategory) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idArticle;
        result = 31 * result + idCategory;
        return result;
    }
}
