package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "comment", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(CommentEntityPK.class)
public class CommentEntity {
    private int idUser;
    private int idArticle;

    @Id
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Id
    @Column(name = "id_article")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentEntity that = (CommentEntity) o;

        if (idUser != that.idUser) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUser;
        result = 31 * result + idArticle;
        return result;
    }
}
