package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ArticleCategoryEntityPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private int idArticle;
    private int idCategory;

    @Column(name = "id_article")
    @Id
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Column(name = "id_category")
    @Id
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleCategoryEntityPK that = (ArticleCategoryEntityPK) o;

        if (idArticle != that.idArticle) return false;
        if (idCategory != that.idCategory) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idArticle;
        result = 31 * result + idCategory;
        return result;
    }
}
