package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "category_technology", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(CategoryTechnologyEntityPK.class)
public class CategoryTechnologyEntity {
    private int idTechnology;
    private int idCategory;

    @Id
    @Column(name = "id_technology")
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Id
    @Column(name = "id_category")
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryTechnologyEntity that = (CategoryTechnologyEntity) o;

        if (idTechnology != that.idTechnology) return false;
        if (idCategory != that.idCategory) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTechnology;
        result = 31 * result + idCategory;
        return result;
    }
}
