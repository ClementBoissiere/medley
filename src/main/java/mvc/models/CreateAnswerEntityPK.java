package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CreateAnswerEntityPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private int idUser;
    private int idArticle;

    @Column(name = "id_user")
    @Id
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Column(name = "id_article")
    @Id
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreateAnswerEntityPK that = (CreateAnswerEntityPK) o;

        if (idUser != that.idUser) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUser;
        result = 31 * result + idArticle;
        return result;
    }
}
