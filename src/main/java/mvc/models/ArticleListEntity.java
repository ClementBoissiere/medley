package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "article_list", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(ArticleListEntityPK.class)
public class ArticleListEntity {
    private int idList;
    private int idArticle;

    @Id
    @Column(name = "id_list")
    public int getIdList() {
        return idList;
    }

    public void setIdList(int idList) {
        this.idList = idList;
    }

    @Id
    @Column(name = "id_article")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleListEntity that = (ArticleListEntity) o;

        if (idList != that.idList) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idList;
        result = 31 * result + idArticle;
        return result;
    }
}
