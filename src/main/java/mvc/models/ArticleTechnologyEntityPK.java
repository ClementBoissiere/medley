package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ArticleTechnologyEntityPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private int idTechnology;
    private int idArticle;

    @Column(name = "id_technology")
    @Id
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Column(name = "id_article")
    @Id
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleTechnologyEntityPK that = (ArticleTechnologyEntityPK) o;

        if (idTechnology != that.idTechnology) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTechnology;
        result = 31 * result + idArticle;
        return result;
    }
}
