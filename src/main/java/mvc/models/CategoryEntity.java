package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "category", schema = "MEDLEY_BD_OSS3", catalog = "")
public class CategoryEntity {
    private int idCategory;
    private String nameCategory;
    private String descCategory;

    @Id
    @Column(name = "id_category")
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Basic
    @Column(name = "name_category")
    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    @Basic
    @Column(name = "desc_category")
    public String getDescCategory() {
        return descCategory;
    }

    public void setDescCategory(String descCategory) {
        this.descCategory = descCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryEntity that = (CategoryEntity) o;

        if (idCategory != that.idCategory) return false;
        if (nameCategory != null ? !nameCategory.equals(that.nameCategory) : that.nameCategory != null) return false;
        if (descCategory != null ? !descCategory.equals(that.descCategory) : that.descCategory != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCategory;
        result = 31 * result + (nameCategory != null ? nameCategory.hashCode() : 0);
        result = 31 * result + (descCategory != null ? descCategory.hashCode() : 0);
        return result;
    }
}
