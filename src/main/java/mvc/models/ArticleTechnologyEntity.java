package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "article_technology", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(ArticleTechnologyEntityPK.class)
public class ArticleTechnologyEntity {
    private int idTechnology;
    private int idArticle;

    @Id
    @Column(name = "id_technology")
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Id
    @Column(name = "id_article")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleTechnologyEntity that = (ArticleTechnologyEntity) o;

        if (idTechnology != that.idTechnology) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTechnology;
        result = 31 * result + idArticle;
        return result;
    }
}
