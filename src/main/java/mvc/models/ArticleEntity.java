package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "article", schema = "MEDLEY_BD_OSS3", catalog = "")
public class ArticleEntity {
    private int idArticle;
    private String title;
    private String content;
    private Integer idUser;
    private Integer idDifficulty;

    @Id
    @Column(name = "id_article")
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "id_user")
    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "id_difficulty")
    public Integer getIdDifficulty() {
        return idDifficulty;
    }

    public void setIdDifficulty(Integer idDifficulty) {
        this.idDifficulty = idDifficulty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleEntity that = (ArticleEntity) o;

        if (idArticle != that.idArticle) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (idUser != null ? !idUser.equals(that.idUser) : that.idUser != null) return false;
        if (idDifficulty != null ? !idDifficulty.equals(that.idDifficulty) : that.idDifficulty != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idArticle;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (idUser != null ? idUser.hashCode() : 0);
        result = 31 * result + (idDifficulty != null ? idDifficulty.hashCode() : 0);
        return result;
    }
}
