package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ArticleListEntityPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private int idList;
    private int idArticle;

    @Column(name = "id_list")
    @Id
    public int getIdList() {
        return idList;
    }

    public void setIdList(int idList) {
        this.idList = idList;
    }

    @Column(name = "id_article")
    @Id
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleListEntityPK that = (ArticleListEntityPK) o;

        if (idList != that.idList) return false;
        if (idArticle != that.idArticle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idList;
        result = 31 * result + idArticle;
        return result;
    }
}
