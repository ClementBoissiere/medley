package mvc.utils;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class ConnectionDB {

    private static SessionFactory factory;
    Session session;
    
    private ConnectionDB()
    {}
     
    private static ConnectionDB INSTANCE = null;
     
    public static ConnectionDB getInstance()
    {           
        if (INSTANCE == null)
        {   INSTANCE = new ConnectionDB(); 
        }
        return INSTANCE;
    }

    public Session openConnection() {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        session = factory.openSession();
        return session;
    }

    public void closeConnection() {
        factory.close();
    }

}
