package mvc.actions;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mvc.dao.ListDAO;
import mvc.models.CategoryEntity;
import mvc.models.ListEntity;

public class ListAction {

	ListDAO listDAO = new ListDAO();
    ObjectMapper objectMapper = new ObjectMapper();
    
    public String getList(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(listDAO.getList());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public String getListById(Integer id){
    	String myJsonString = "";
    		try {
    			myJsonString = objectMapper.writeValueAsString(listDAO.getListById(id));
    		} catch (JsonProcessingException e) {
    			e.printStackTrace();
    		}
    	return myJsonString;
    }
    
    public String getArticleByList(int id){
        ObjectMapper objectMapper = new ObjectMapper();
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(listDAO.getArticleByList(id));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public void createList(String myJsonUser) {
        ListEntity newList = new ListEntity();
        try {
        	newList = objectMapper.readValue(myJsonUser, ListEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        listDAO.createList(newList);
    }
    
    public void deleteListById(int id) {
    	if(this.getListById(id) != null)
		{
    		listDAO.deleteList(id);
		}
    }
    
    public void updateList(String myJsonUser, Integer id) {
    	ListEntity list = new ListEntity();
        try {
        	list = objectMapper.readValue(myJsonUser, ListEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        list.setIdList(id);
        listDAO.updateList(list);
    }
	
}
