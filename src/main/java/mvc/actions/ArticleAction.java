package mvc.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mvc.dao.ArticleDAO;

public class ArticleAction {
    ArticleDAO articleDAO = new ArticleDAO();
    ObjectMapper objectMapper = new ObjectMapper();

    public String getArtciles(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(articleDAO.getArticles());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }

    public String getArticleById(Integer id){
        String myJsonString = "";
        try {
            myJsonString = objectMapper.writeValueAsString(articleDAO.getArcticleById(id));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
}
