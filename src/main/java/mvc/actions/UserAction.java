package mvc.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mvc.dao.UserDAO;
import mvc.models.UsersEntity;

import java.io.IOException;

public class UserAction {
	
        UserDAO userDAO = new UserDAO();
        ObjectMapper objectMapper = new ObjectMapper();

        public String getUsers(){
            String myJsonString = "";
            try {
                myJsonString = objectMapper.writeValueAsString(userDAO.getUsers());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return myJsonString;
        }

        public String getUserById(Integer id){
            String myJsonString = "";
            try {
                myJsonString = objectMapper.writeValueAsString(userDAO.getUserById(id));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return myJsonString;
        }
        
        public String getListByUser(Integer id){
            String myJsonString = "";
            try {
                myJsonString = objectMapper.writeValueAsString(userDAO.getListByUser(id));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return myJsonString;
        }

        public void createUser(String myJsonUser) {
            UsersEntity newUser = new UsersEntity();
            try {
                newUser = objectMapper.readValue(myJsonUser, UsersEntity.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            userDAO.createUser(newUser);
        }

        public void deleteUserById(int id) {
        	if(this.getUserById(id) != null)
			{
			    userDAO.deleteUser(id);
			}
        }

        public void updateUser(String myJsonUser, Integer id) {
            UsersEntity user = new UsersEntity();
            try {
                user = objectMapper.readValue(myJsonUser, UsersEntity.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            user.setIdUser(id);
            userDAO.updateUser(user);
        }

}
