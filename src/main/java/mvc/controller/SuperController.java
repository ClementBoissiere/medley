package mvc.controller;

import mvc.actions.UserAction;
import mvc.actions.ArticleAction;
import mvc.actions.CategoryAction;
import mvc.actions.ListAction;
import mvc.actions.TechnologyAction;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import java.net.URI;


@Path("/ws")
public class SuperController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final URI BASE_URI = getBaseURI();

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/rest/").build();
	}

    //
    // ARTICLE REQUEST RESEIVER
	//
	
	@GET()
    @Path("/article")
    @Produces(MediaType.APPLICATION_JSON)
    public String getArticle() {
        ArticleAction actionArticle = new ArticleAction();
        return actionArticle.getArtciles();
    }

    @GET()
    @Path("/article/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getArticleById(@PathParam("id") Integer id){
	    ArticleAction actionArticle = new ArticleAction();
	    return actionArticle.getArticleById(id);
    }
    
    //
    // TECHNOLOGY REQUEST RESEIVER
	//
    
	@GET()
    @Path("/technology")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTechnos() {
		TechnologyAction getTechnoAction = new TechnologyAction();
        return getTechnoAction.getTechnos();
    }
	
    @GET()
    @Path("/technology/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTechnoById(@PathParam("id") Integer id){
    	TechnologyAction technoPerCat = new TechnologyAction();
	    return technoPerCat.getTechnoById(id);
    }
    
    @GET()
    @Path("/categorybytechno/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategorieByTechno(@PathParam("id") Integer id){
    	TechnologyAction CategoryPerTechno = new TechnologyAction();
	    return CategoryPerTechno.getCategoryByTechno(id);
    }
    
    @POST()
    @Path("/technology/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createTechno(String request) {
    	TechnologyAction createTechno = new TechnologyAction();
    	createTechno.createTechno(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/technology/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTechnoById(@PathParam("id") Integer id) {
    	TechnologyAction deleteTechno = new TechnologyAction();
    	deleteTechno.deleteTechnoById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/technology/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTechnoById(@PathParam("id") Integer id,String request) {
    	TechnologyAction updateTechno = new TechnologyAction();
    	updateTechno.updateTechno(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //
    // CATEGORY REQUEST RESEIVER
	//
    
	@GET()
    @Path("/category")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategorie() {
		CategoryAction getCategorieAction = new CategoryAction();
        return getCategorieAction.getCategory();
    }

    @GET()
    @Path("/category/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategorieById(@PathParam("id") Integer id){
    	CategoryAction technoPerCat = new CategoryAction();
	    return technoPerCat.getCategoryById(id);
    }
    
    @GET()
    @Path("/technobycategory/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTechnoByCategorie(@PathParam("id") Integer id){
    	CategoryAction technoPerCat = new CategoryAction();
	    return technoPerCat.getTechnoByCategory(id);
    }
    
    @POST()
    @Path("/category/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createCategory(String request) {
        CategoryAction actionUser = new CategoryAction();
        actionUser.createCategory(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/category/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCategoryById(@PathParam("id") Integer id) {
    	CategoryAction deleteCat = new CategoryAction();
    	deleteCat.deleteCategoryById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/category/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCategoryById(@PathParam("id") Integer id,String request) {
    	CategoryAction updateCat = new CategoryAction();
    	updateCat.updateCategory(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //
    // USER REQUEST RESEIVER
    //
    
    @GET()
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers() {
        UserAction actionUser = new UserAction();
        return actionUser.getUsers();
    }

    @GET()
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsersById(@PathParam("id") Integer id) {
        UserAction actionUser = new UserAction();
        return actionUser.getUserById(id);
    }
    
    @GET()
    @Path("/userlist/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getListByUser(@PathParam("id") Integer id) {
        UserAction actionUser = new UserAction();
        return actionUser.getListByUser(id);
    }

    @POST()
    @Path("/user/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createUser(String request) {
        UserAction actionUser = new UserAction();
        actionUser.createUser(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/user/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUserById(@PathParam("id") Integer id) {
        UserAction userPerCat = new UserAction();
        userPerCat.deleteUserById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/user/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserById(@PathParam("id") Integer id,String request) {
        UserAction actionUser = new UserAction();
        actionUser.updateUser(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //
    // LIST REQUEST RESEIVER
	//
    
	@GET()
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public String getList() {
		ListAction getListAction = new ListAction();
        return getListAction.getList();
    }

    @GET()
    @Path("/list/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getListById(@PathParam("id") Integer id){
    	ListAction listList = new ListAction();
	    return listList.getListById(id);
    }
    
    @GET()
    @Path("/articlebylist/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getArticleByList(@PathParam("id") Integer id){
    	ListAction articleByList = new ListAction();
	    return articleByList.getArticleByList(id);
    }
    
    @POST()
    @Path("/list/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createList(String request) {
    	ListAction createList = new ListAction();
    	createList.createList(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/list/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteListById(@PathParam("id") Integer id) {
    	ListAction deleteList = new ListAction();
    	deleteList.deleteListById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/list/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateListById(@PathParam("id") Integer id,String request) {
    	ListAction updateList = new ListAction();
    	updateList.updateList(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
}

